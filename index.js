const topNStudentsAttendees = (students, attendees, N) => {
    let students_attendance = [];
    students.forEach(student => {
        students_attendance.push({
            name: student,
            attendees: 0
        });
    });
    attendees.forEach(lecture => {
        let lecture_attendees = new Set(lecture);
        lecture_attendees.forEach(attendee => {
            let student_attendance = students_attendance.filter(student => student.name === attendee)[0];
            if (student_attendance) {
                student_attendance.attendees++;
            }
        });
    })
    students_attendance.sort((a, b) => {
        return a.attendees < b.attendees ? 1 : -1
    });
    return students_attendance.splice(0, N).map((student) => student.name);
}